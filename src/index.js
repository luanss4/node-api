const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res)=>{
    res.send(`API rodando na porta ${port}`)
})

app.listen(3000, ()=>{
    console.log('API Iniciada');
});